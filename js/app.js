"use strict"

/**
 * Lazy load
 */

const lazyLoadElements = document.querySelectorAll('.lazy-load')

for (const _lazyLoadElement of lazyLoadElements) {
    const loaded = () => {
        window.setTimeout(() => {
            _lazyLoadElement.classList.add('loaded')
        }, Math.random() * 1000)
    }

    if (_lazyLoadElement.complete) {
        loaded()
    }
    else {
        _lazyLoadElement.addEventListener('load', loaded)
    }
}

/**
 * Reveal facto width
 */

const articles = document.querySelectorAll("article")
const width = window.innerWidth

function size(n){
    for (let i = 0; i < n; i++) {
        articles[i].classList.remove("reveal")
    }
}
if( width >= "1200" ){
    size(6)
} else if (width >= "575"){
    size(4)
} else {
    size(2)
}


/**
 * Reveal
 */

const revealItems = []

for (const _element of articles) {
    const item = {}
    item.revealed = false
    item.element = _element

    const bounding = _element.getBoundingClientRect()
    item.top = bounding.top + scrollY
    item.height = bounding.height

    revealItems.push(item)
}

window.addEventListener('resize', () => {
    const scrollY = window.scrollY

    for (const _item of revealItems) {
        const bounding = _item.element.getBoundingClientRect()
        _item.top = bounding.top + scrollY
        _item.height = bounding.height
    }
})

window.addEventListener("scroll", () => {

    const limit = window.scrollY + window.innerHeight

    for (const _item of revealItems) {

        if (!_item.revealed && limit > _item.top + _item.height * 0.4) {
            _item.revealed = true
            _item.element.classList.add("revealed")
        }
    }

})


/**
 * Slider
 */

let slideIndex = [1, 1, 1]
const slideId = ["slider1", "slider2", "slider3"]
showSlides(1, 0)
showSlides(1, 1)
showSlides(1, 2)

function plusSlides(e, n) {
    showSlides(slideIndex[n] += e, n)
}

function showSlides(e, n) {
    var i;
    const slides = document.getElementsByClassName(slideId[n])
    if (e > slides.length) {
        slideIndex[n] = 1
    }
    if (e < 1) {
        slideIndex[n] = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none"
    }
    slides[slideIndex[n] - 1].style.display = "block"
}


/**
 * dropdown
 */

const dest = document.querySelector(".destinations")
const dropDown = document.querySelector(".dropDown")
const select = document.querySelector(".selector")

select.addEventListener("click", (e) => {
    dropDown.style.display = "flex";
    e.stopPropagation()
});

document.body.addEventListener("click", (e) => {
    if (!dest.contains(e.target)) {
        dropDown.style.display = "none";
    }
});
